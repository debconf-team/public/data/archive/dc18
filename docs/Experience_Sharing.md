# Experience Sharing

## Wiki

*   Prepare a [wiki page](https://wiki.debconf.org/wiki/DebConf18/Bids/Hsinchu).
*   All price information shall be in USD.

## Decision Meeting

*   Prepare a [status page](https://wiki.debconf.org/wiki/DebConf18/Bids/Hsinchu/Status).
